# Notification Manager

Notification Manager written in Tornado _ is a Python web framework and making it for
client connections and publish the messages on connected clients through WebSocket.

## Getting Started
```
$ virtualenv -p python3 newenv
$ source newenv/bin/activate
$ pip3 install -r requirements.txt

```
Start the server with following command
```
$ python3 server.py

```

Examples: Create the connections for clients and publish the message

```

$ python3 examples/clientA.py
$ python3 examples/clientB.py


curl -X POST \
  http://127.0.0.1:8888/publish \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 5acbb16f-33af-41ed-8068-1f176344e0af' \
  -H 'cache-control: no-cache' \
  -d '{
	"client_id":"1234",
	"message":"Hello world"
}'


Reference:
-------------
https://github.com/tornadoweb/tornado/tree/stable/demos/websocket
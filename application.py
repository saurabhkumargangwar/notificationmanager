import tornado

from urls import urls


class Application(tornado.web.Application):
	def __init__(self):
		super(Application, self).__init__(urls)

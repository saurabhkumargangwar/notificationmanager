from handler.api.client.connection import ConnectionHandler
from handler.api.publisher.publisher import PublisherHandler, DemoHandler

urls = [
	(r"/notifications/demo", DemoHandler),
	(r"/notifications/publish", PublisherHandler),  # To publish the message
	(r"/notifications/client", ConnectionHandler)  # To connect the clients
]

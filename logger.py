import logging
import uuid

global file_path

file_path = '/home/usher/logs/notificationmanager/application.log'

logger = logging.getLogger(__name__)

# set log level
logger.setLevel(logging.INFO)

class RequestFilter(logging.Filter):
    def filter(self, record):
        record.request_id = str(uuid.uuid1())
        return super(RequestFilter, self).filter(record)

file_handler = logging.FileHandler(file_path)
formatter    = logging.Formatter('%(asctime)s :: %(levelname)s :: Module %(module)s :: Line No %(lineno)s :: %(message)s')
file_handler.setFormatter(formatter)

# add file handler to logger
logger.addHandler(file_handler)

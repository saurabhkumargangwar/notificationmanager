from tornado.websocket import WebSocketClosedError

from handler.api.client.connection import clients, rdb
from .utils import handle_exceptions
from logger import logger

@handle_exceptions
def publish_message(account_id, user_id, message):
	"""
	This method used to send message to active clients corresponding to client_id
	:param user_id:
	:param message:
	:return:
	"""
	connection_key = account_id + "_" + user_id
	connection_ids = rdb.lrange(connection_key, 0, -1)
	if connection_ids:
		for connection_id in connection_ids:
			if clients.get(connection_id.decode('utf-8')):
				connection = clients[connection_id.decode('utf-8')]
				logger.info("Sending notification to : %s.", connection_key)
				connection.write_message(message)
			else:
				logger.info("There is no active connection found in memory for account_id %s and user_id %s.",
							account_id, user_id)
		logger.info("Notification sent to : %s successfully.", connection_key)
		return True
	else:
		logger.info("There is no active connection found into redis for account_id %s and user_id %s.",
					account_id, user_id)
		return False
	return False

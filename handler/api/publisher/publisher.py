import json
import tornado
from handler.api.helper import publish_message
from logger import logger

class BaseHandler(tornado.web.RequestHandler):
	def prepare(self):
		logger.info("Request to connect web socket:  %s  %s \n Host: %s \n Host Name: %s \n Remote Ip: %s \n "
					"Protocol: %s \n Path: %s \n Query Arguments: %s \n Request Body: %s.\n Headers %s",
					self.request.method, self.request.uri, self.request.host,
					self.request.host_name, self.request.remote_ip, self.request.protocol,
					self.request.path, self.request.query_arguments, self.request.body,
					json.dumps(self.request.headers._dict, sort_keys=True, indent=2))

class PublisherHandler(BaseHandler):
	"""
	This class used to send the message to all active clients(connected to WebSocket server)
	"""

	def post(self):
		"""
		POST URL: /publish
		Defined method to publish the message for clients.
		request_body: {
		"user_id": "user_id",
		"message": "I am message"
		}
		:return:
		"""
		data = json.loads(self.request.body)
		user_id = data['user_id']
		account_id = data['account_id']
		message = data['message']

		logger.info("Publishing message %s to user_id %s.", message, user_id)
		is_message_published = publish_message(account_id, user_id, message)  # publish message to client handler
		if is_message_published:
			logger.info("Message %s is published successfully to user_id %s.", message, user_id)
			self.write({'message': 'data published.'})
			self.set_status(200)
		else:
			logger.info("Server is not able to publish message %s to user_id %s.", message, user_id)
			self.write({'message':'Server is not able to publish message to client.'})
			self.set_status(400)





class DemoHandler(BaseHandler):
	"""
	This class used to send the message to all active clients(connected to WebSocket server)
	"""

	def get(self):
		self.render("index.html")
import json

import tornado
import redis
from tornado.options import define, options
from handler.api.utils import generate_connection_id
from logger import logger

global clients
clients = {}  # Dictionary: To store active connection list wrt client_id.

define("REDIS_HOST", default="localhost", help="redis run on host", type=str)
define("REDIS_PORT", default="6379", help="redis run on port", type=str)

rdb = redis.StrictRedis(host=options.REDIS_HOST, port=options.REDIS_PORT, db=0)

class BaseHandler(tornado.websocket.WebSocketHandler):
	def prepare(self):
		logger.info("Request to connect web socket:  %s  %s \n Host: %s \n Host Name: %s \n Remote Ip: %s \n "
					"Protocol: %s \n Path: %s \n Query Arguments: %s \n Request Body: %s.\n Headers %s",
					self.request.method, self.request.uri, self.request.host,
					self.request.host_name, self.request.remote_ip, self.request.protocol,
					self.request.path, self.request.query_arguments, self.request.body,
					json.dumps(self.request.headers._dict, sort_keys=True, indent=2))

class ConnectionHandler(BaseHandler):
	"""
	This class used to create and close WebSocket connections.
	Call `open` method to open the WebSocket connections from client
	"""

	def open(self):
		"""
		Defined WebSocket url to open a WebSocket connection from client
		url: ws://localhost:8888/client?user_id=1234

		Example:
		# var ws = new WebSocket("ws://localhost:8888/client?user_id=1234&account_id");
		# ws.onopen = function() {
		# 	ws.send("Hello, world");
		#   };
		#   ws.onmessage = function (evt) {
		# 	 alert(evt.data);
		#   };

		This script pops up an alert box that says "You said: Hello, world".
		"""
		user_id = self.request.query_arguments['user_id'][0].decode("utf-8")
		account_id = self.request.query_arguments['account_id'][0].decode("utf-8")

		connection_key = account_id + "_" + user_id
		connection_id = generate_connection_id()

		logger.info("connection_id : %s  is created for user_id: %s and account_id: %s.", connection_id, user_id, account_id)
		self.id = connection_id
		rdb.lpush(connection_key, connection_id)
		rdb.set(connection_id, connection_key, 24*7*60*60)
		clients[connection_id] = self

	def on_close(self):
		"""
		This method is invoked when client closes the WebSocket connection.
		:return:
		"""
		clients[self.id] = None
		connection_key = rdb.get(self.id)
		rdb.lrem(connection_key, 1, self.id)
		rdb.delete(self.id)

	def check_origin(self, origin):
		#parsed_origin = urllib.parse.urlparse(origin)
		return True


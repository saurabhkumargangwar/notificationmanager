import uuid
from functools import wraps
from logger import logger

from tornado.websocket import WebSocketClosedError


def handle_exceptions(fn):
	@wraps(fn)
	def wrapper(*args, **kwargs):
		try:
			return fn(*args, **kwargs)
		except WebSocketClosedError as e:
			logger.error("WebSocketClosed error e %s", e)
			return False
		except ValueError as message:
			logger.error(message)
			#return abort(400, message=message)
		except KeyError as message:
			logger.error(message)
			#return abort(400, message=message)
	return wrapper



def generate_connection_id():
	logger.info("Generating UUID for client connection.")
	random_id = str(uuid.uuid1())
	logger.info("Generating random id : %s to create connection.", random_id)
	return random_id
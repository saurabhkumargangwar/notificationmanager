import tornado.escape
import tornado.ioloop
import tornado.options
import logger
from tornado.options import define, options
from application import Application

define("PORT", default="5000", help="redis run on host", type=str)
define("env", default="local", help="redis run on host", type=str)

def main():
	tornado.options.parse_command_line(final=False)
	env = options.env
	#logger.info("Server is starting in %s environment", env)
	if env=='prod':
		tornado.options.parse_config_file("config/prod.conf")
	elif env=='dev':
		tornado.options.parse_config_file("config/dev.conf")
	else:
		tornado.options.parse_config_file("config/local.conf")

	app = Application()
	app.listen(options.PORT)
	tornado.ioloop.IOLoop.current().start()
	logger.info("Server started successfully on port %s.", options.PORT)


if __name__ == "__main__":
	main()
